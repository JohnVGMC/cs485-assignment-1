# CS485Assignment1
#### John Calderon
#### CS 485
#### Xin Ye

Unity Assets for the Roll-A-Ball Tutorial Game and Custom Game (based off the 2D UFO Tutorial Game)

### How To Run
By either cloning or downloading the repository, and building the game through Unity.  
Or you can download the game on its own here: [Download Link](https://drive.google.com/file/d/1V3sDn8STnU82R-t25-Apg6lcw6JkDzeX/view?usp=sharing).

More info about the game, as well as well as the assignment can be found under [Assignment1Report.pdf](Assignment1Report.pdf).

With a working Main Menu, you can simply click which game you want to play. The "i" buttons next to each game will tell you how to play their respective games. Both games also have a button to take you back to the main menu.

While Unity's standalone player allows for custom controls, the default controls are either the directional keys, or the "WASD" controls to move the player object on both games.

WASD Controls:  
- W = Up  
- A = Left  
- S = Down  
- D = Right
